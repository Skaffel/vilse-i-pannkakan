using Vilse;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WorldGenerator : MonoBehaviour {
	
	public Transform Lamp;
	
	public Transform Ground;
	public Transform Wall;
	
	public Transform Goal;
	
	public Transform Player;
	public Transform Predator;
	
	public Vector2 size = new Vector2(10, 10);
	public Vector2 cellSize = new Vector2(1, 1); 
		
	private Grid grid;
	
	// Use this for initialization
	void Awake() {	
		grid = new Grid(size, cellSize);
		
		// Create maze
		createMaze();
		
		// Create walls
		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {
				if (grid.getDirections(new Vector2(x, y)).Count > 0) {
					createWall(new Vector2(x, y), new Vector2( 1, 0));
					createWall(new Vector2(x, y), new Vector2(-1, 0));
					createWall(new Vector2(x, y), new Vector2(0,  1));
					createWall(new Vector2(x, y), new Vector2(0, -1));
					
					createGround(new Vector2(x, y));
					
					//if (Random.Range(0, 100) > 95)
						//createLamp(new Vector2(x, y));
				}
			}
		}
		Vector2 goalPos = new Vector2(Random.Range(0, (int) (size.x / 2)) * 2, Random.Range(0, (int) (size.y / 2)) * 2);
		Vector2 playerPos = new Vector2(Random.Range(0, (int) (size.x / 2)) * 2, Random.Range(0, (int) (size.y / 2)) * 2);
		
		// Place goal
		createGoal(goalPos);
		
		// Place player
		createPlayer(playerPos);
		
		// Create predator
		for (int n = 0; n < 3; n++) {
			createPredator(new Vector2(Random.Range(0, (int) (size.x / 2)) * 2, Random.Range(0, (int) (size.y / 2)) * 2));
		}
	}
	
	private void createMaze() {
		int totalConnections = 2 * (int) ((size.x / 2 - 1) * (size.y / 2 - 1));
		int currentConnections = 0;
		
		int resolution = 2;
		int[,] visited = new int[(int) size.x, (int) size.y];
		
		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {
				visited[x, y] = 1;
			}
		}
		
		List<Vector2> stack = new List<Vector2>();
		
		stack.Add(new Vector2(Mathf.Round(cellSize.x / 2), Mathf.Round(cellSize.y / 2)));
		
		while (stack.Count > 0) {
			Vector2 position = stack[stack.Count - 1];
			visited[(int) Mathf.Round(position.x), (int) Mathf.Round(position.y)] -= 1;
			
			List<Vector2> directions = new List<Vector2>();
			
			directions.Add(new Vector2( resolution, 0));
			directions.Add(new Vector2(-resolution, 0));
			directions.Add(new Vector2(0,  resolution));
			directions.Add(new Vector2(0, -resolution));
			
			Vector2 newPosition = new Vector2(-1, -1);
			Vector2 direction = new Vector2();
			bool deadEnd = false;
			
			while (!grid.validCell(newPosition) && !deadEnd) {
				int choice = Random.Range(0, directions.Count);
				
				newPosition = position + directions[choice];
				direction = directions[choice];
				
				if (!grid.validCell(newPosition) || visited[(int) Mathf.Round(newPosition.x), (int) Mathf.Round(newPosition.y)] < 1) {
					newPosition.Set(-1, -1);
					
					directions.RemoveAt(choice);
				}
				
				if (directions.Count == 0)
					deadEnd = true;
			}
			
			if (deadEnd) {
				stack.RemoveAt(stack.Count - 1);
			}
			else {
				Vector2 hallPosition = position;
				
				for (int n = 0; n < resolution; n++) {
					grid.connectCells(hallPosition, hallPosition + direction.normalized);
					
					hallPosition += direction.normalized;
				}
				
				currentConnections++;
				
				stack.Add(newPosition);
			}
		}
		
		while((currentConnections / (float) totalConnections) < 0.8){
			int x = Random.Range(1, (int) (size.x / 2) - 1) * 2;
			int y = Random.Range(1, (int) (size.y / 2) - 1) * 2;
			
			Vector2 position = new Vector2(x, y);
			
			List<Vector2> directions = new List<Vector2>();
			
			directions.Add(new Vector2( resolution, 0));
			directions.Add(new Vector2(-resolution, 0));
			directions.Add(new Vector2(0,  resolution));
			directions.Add(new Vector2(0, -resolution));
			
			int choice = Random.Range(0, directions.Count);
			
			Vector2 direction = directions[choice];
			
			if (grid.validCell(position + direction) && !grid.hasDirection(position, direction)) {
				Vector2 hallPosition = position;
				
				for (int n = 0; n < resolution; n++) {
					grid.connectCells(hallPosition, hallPosition + direction.normalized);
					
					hallPosition += direction.normalized;
				}
				
				currentConnections++;
			}
		}
		
		Debug.Log("Difficulty: " + (currentConnections / (float) totalConnections));
	}
	
	private void createWall(Vector2 position, Vector2 direction) {
		if (!grid.hasDirection(position, direction)) {
			Quaternion rotation = Quaternion.Euler(- 90 * direction.y, 0, 0);
			
			if (direction.x != 0)
				rotation = Quaternion.Euler(90, - direction.x * 90, 0);
			
			Instantiate(
				Wall, 
				new Vector3(position.x * cellSize.x + direction.x * 2.5f, 1, position.y * cellSize.y + direction.y * 2.5f), 
				rotation);
		}
	}
	
	private void createGround(Vector2 position) {
		Instantiate(
				Ground, 
				new Vector3(position.x * cellSize.x, -4, position.y * cellSize.y), 
				Quaternion.identity);
	}
	
	private void createLamp(Vector2 position) {
		Quaternion rotation = Quaternion.identity;
		
		rotation.Set(1, 0, 0, 1);
		
		Instantiate(
				Lamp, 
				new Vector3(position.x * cellSize.x, 3, position.y * cellSize.y), 
				rotation);
	}
	
	private void createGoal(Vector2 position) {
		Debug.Log("Goal position: " + position);
		
		Quaternion rotation = Quaternion.identity;
		rotation.Set(-1, 0, 0, 1);
		
		Object goal = Instantiate(
						Goal, 
						new Vector3(position.x * cellSize.x, 0, position.y * cellSize.y), 
						rotation);
		
		goal.name = "Goal";
	}
	
	private void createPlayer(Vector2 position) {
		Object player = Instantiate(
							Player, 
							new Vector3(position.x * cellSize.x, 1, position.y * cellSize.y), 
							Quaternion.identity);
		
		player.name = "A Player";
	}
	
	private void createPredator(Vector2 position) {
		Object predator = Instantiate(
							Predator, 
							new Vector3(position.x * cellSize.x, 0, position.y * cellSize.y), 
							Quaternion.identity);
		
		predator.name = "A Predator";
	}
	
	public Grid getGrid() {
		return grid;
	}	
}
