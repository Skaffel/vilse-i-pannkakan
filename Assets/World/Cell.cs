using UnityEngine;
using System.Collections.Generic;

namespace Vilse {
	public class Cell {
		public Vector2 position;
		public List<Cell> connectedCells;
		
		public List<SoundWave> soundWaves;
		public List<SoundWave> staticWaves;
		
		public Cell(Vector2 position) {
			this.position = position;
			this.connectedCells = new List<Cell>();
			
			this.soundWaves = new List<SoundWave>();
			this.staticWaves = new List<SoundWave>();
		}
		
		public bool addSoundWave(SoundWave soundWave) {
			if (soundWave.loop)
				return addStaticSoundWave(soundWave);
			else
				soundWaves.Add(soundWave);
				
			return true;
		}
		
		private bool addStaticSoundWave(SoundWave soundWave) {
			if (staticWaves.Count < 5) {
				staticWaves.Add(soundWave);
				
				return true;
			}
				
			return false;
		}
		
		public bool isConnected(Cell cell) {
			foreach (Cell connectedCell in connectedCells) {
				if (cell == connectedCell) {
					return true;
				}
			}
			
			return false;
		}
	}
}

