using UnityEngine;
using System;
using System.Collections.Generic;

namespace Vilse {
	public class Grid {
		public Vector2 size;
		public Vector2 cellSize;
		public Cell[,] cells;
		
		public Grid(Vector2 size, Vector2 cellSize) {
			this.size = size;
			this.cellSize = cellSize;
			this.cells = new Cell[(int)size.x, (int)size.y];
			
			for (int x = 0; x < size.x; x++) {
				for (int y = 0; y < size.y; y++) {
					cells[x, y] = new Cell(new Vector2(x, y));
				}
			}
		}
		
		// Get directions
		public List<Vector2> getDirections(Vector2 position) {
			return getDirections(getCell(position));
		}
		
		public List<Vector2> getDirections(Cell cell) {
			List<Cell> connectedCells = cell.connectedCells;
			List<Vector2> directions = new List<Vector2>();
			
			for (int n = 0; n < connectedCells.Count; n++) {
				directions.Add(connectedCells[n].position - cell.position);
			}
			
			return directions;
		}
		
		public bool hasDirection(Vector2 position, Vector2 direction) {
			if (!validCell(position) || !validCell(position + direction))
				return false;
			return hasDirection(getCell(position), getCell(position + direction));
		}
		
		public bool hasDirection(Cell cell, Cell targetCell) {
			return cell.isConnected(targetCell);
		}
		
		// Connect cells
		public void connectCells(Vector2 a, Vector2 b) {
			connectCells(getCell(a), getCell(b));
		}
		
		public void connectCells(Cell a, Cell b) {
			a.connectedCells.Add(b);
			b.connectedCells.Add(a);
		}
	
		public Cell getCell(Vector2 position) {
			return cells[(int) Math.Round(position.x), (int) Math.Round(position.y)];
		}
			
		public bool validCell(Vector2 position) {
			return 0 <= position.x && position.x + 0.5 < size.x && 0 <= position.y && position.y + 0.5 < size.y;
		}
	}
}

