using UnityEngine;
using System;

namespace Vilse {
	public class Utilities {
		public static Vector2 getGridPosition(Vector2 position) {
			return new Vector2(Mathf.Round(position.x), Mathf.Round(position.y));
		}
		
		public static T getComponent<T>(String gameObject) where T : Component {
			GameObject obj = GameObject.Find(gameObject);
			
			if (obj == null)
				return null;
			return obj.GetComponent<T>();
		}
		
		public static double getBufferLength() {
			return 0.02 + Time.deltaTime;
		}
	}
}

