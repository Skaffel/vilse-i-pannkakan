using Vilse;
using UnityEngine;
using System;
using System.Collections.Generic;

public class SoundSimulation : MonoBehaviour {
	
	public double loopModifier = 0.1;
	public int bounceModifier = 10;
	public double soundSpeed = 50;
	public double wallSoundDamping = 0.5;
	public double soundDecay = 0.9;
	
	public double minIntensity = 0.001;
	
	private Grid grid; 
	
	private List<SoundWave> soundWaves;
	
	private double sqrt2 = Mathf.Sqrt(2);
	
	void Awake() {
		soundWaves = new List<SoundWave>();
	}
	
	// Use this for initialization
	void Start() {
		GameObject worldGenerator = GameObject.Find("World");
		
		grid = worldGenerator.GetComponent<WorldGenerator>().getGrid();
	}
	
	// Update is called once per frame
	void Update() {
		// Remove all old used sound
		for (int x = 0; x < grid.size.x; x++) {
			for (int y = 0; y < grid.size.y; y++) {
				List<SoundWave> cellSoundWaves = grid.cells[x, y].soundWaves;
				
				for (int n = cellSoundWaves.Count - 1; n >= 0; n--) {
					if (Time.time > cellSoundWaves[n].startTime + cellSoundWaves[n].lifeTime + Utilities.getBufferLength()) {
						SoundWave.destroy(cellSoundWaves[n]);
						
						cellSoundWaves.RemoveAt(n);
					}
				}
			}
		}
		
		// Process all the sound waves
		while (soundWaves.Count > 0) {
			stepSoundWave(soundWaves[0]);
		}
	}
	
	public void stepSoundWave(SoundWave soundWave) {
		// This soundwave is being processed so remove it
		soundWaves.RemoveAt(0);
		
		// Add sound wave
		Cell cell = grid.getCell(soundWave.position);
		
		if (!cell.addSoundWave(soundWave))
			soundWave.intensity = 0;
		
		// Move sound wave
		SoundWave movedSoundWave = soundWave.clone();
		
		Vector2 straight = movedSoundWave.direction;
		Vector2 left = new Vector2(-movedSoundWave.direction.y, movedSoundWave.direction.x);
		Vector2 right = new Vector2(movedSoundWave.direction.y, -movedSoundWave.direction.x);
		
		double time = Math.Abs(Vector2.Dot(movedSoundWave.direction, grid.cellSize)) / soundSpeed;
		
		int splitNumber = 0;
		
		bool hasStraight = grid.hasDirection(movedSoundWave.position, straight) ;
		bool hasLeft = grid.hasDirection(movedSoundWave.position, left);
		bool hasRight = grid.hasDirection(movedSoundWave.position, right);
		
		if (hasStraight) splitNumber++;
		if (hasLeft) splitNumber++;
		if (hasRight) splitNumber++;
		
		if (splitNumber == 0) splitNumber = 1;
		
		movedSoundWave.intensity *= soundDecay / Mathf.Sqrt(splitNumber);
		
		if (movedSoundWave.intensity > minIntensity) {
			if (hasLeft) {
				SoundWave newSoundWave = movedSoundWave.clone();			
				newSoundWave.direction = left;
				newSoundWave.position += left;
				newSoundWave.startTime += sqrt2 * time;
				newSoundWave.loops -= soundDecay * loopModifier;
				newSoundWave.intensity /= (1 + Mathf.Abs((float) newSoundWave.loops));
				
				soundWaves.Add(newSoundWave);
			}
		
			if (hasRight) {
				SoundWave newSoundWave = movedSoundWave.clone();
				newSoundWave.direction = right;
				newSoundWave.position += right;
				newSoundWave.startTime += sqrt2 * time;
				newSoundWave.loops += soundDecay * loopModifier;
				newSoundWave.intensity /= (1 + Mathf.Abs((float) newSoundWave.loops));
	
				soundWaves.Add(newSoundWave);
			}
			
			if (hasStraight) {
				movedSoundWave.position += straight;
				movedSoundWave.startTime += time;
				movedSoundWave.loops *= soundDecay * soundDecay;
				
				soundWaves.Add(movedSoundWave);
			}
		}
	}
		
	
	public void addSoundWave(SoundWave soundWave) {
		soundWaves.Add(soundWave);
	}
}
