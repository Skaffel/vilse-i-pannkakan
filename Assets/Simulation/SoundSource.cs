using System;
using UnityEngine;

namespace Vilse {
	public class SoundSource {
		public double intensityModifier;
		public double volumeModifier;
		
		public double startTime; 
		
		public Vector2 startPosition;
		
		public long currentSample = -1;
		
		public SoundSource (double startTime, double intensityModifier, double volumeModifier) {
			this.startTime = startTime;
			
			this.intensityModifier = intensityModifier;
			this.volumeModifier = volumeModifier;
		}
	}
}

