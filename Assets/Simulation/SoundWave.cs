using UnityEngine;
using System;

namespace Vilse {
	public class SoundWave {
		public float[] sound;
		public int sampleRate;
		public int channels;
		
		public SoundSource source;
		
		public Vector2 position;
		public Vector2 direction;
		
		public double intensity;
		
		public double startTime;
		public double lifeTime;
		public bool loop;
		
		public int type;
		
		public long currentSample;
		public long offsetSample;
		
		public int bounces;
		public double loops;
		
		public SoundWave(float[] sound, int sampleRate, int channels, Vector2 position, Vector2 direction, double intensity, double startTime, double lifeTime, bool loop, int type) {
			init(sound, sampleRate, channels, position, direction, intensity, startTime, lifeTime, loop, type);
		}
		
		public SoundWave init(float[] sound, int sampleRate, int channels, Vector2 position, Vector2 direction, double intensity, double startTime, double lifeTime, bool loop, int type) {
			this.sound = sound;
			this.sampleRate = sampleRate;
			this.channels = channels;
			
			this.position = position;
			this.direction = direction;
			
			this.intensity = intensity;
			
			this.startTime = startTime;
			this.lifeTime = lifeTime;
			this.loop = loop;
			
			this.type = type;
			
			currentSample = -1;
			offsetSample = 0;
			
			bounces = 0;
			loops = 0;
			
			return this;
		}
		
		public SoundWave clone() {
			SoundWave newSoundWave = create(sound, sampleRate, channels, position, direction, intensity, startTime, lifeTime, loop, type);
			
			newSoundWave.source = source;
			
			newSoundWave.bounces = bounces;
			newSoundWave.loops = loops;
			
			return newSoundWave;
		}
		
		private static int storedSoundWaves = 0;
		private static SoundWave[] soundWaves;
		
		
		public static SoundWave create(float[] sound, int sampleRate, int channels, Vector2 position, Vector2 direction, double intensity, double startTime, double lifeTime, bool loop, int type) {
			if (soundWaves == null) soundWaves = new SoundWave[1000];
			
			if (storedSoundWaves == 0)
				return new SoundWave(sound, sampleRate, channels, position, direction, intensity, startTime, lifeTime, loop, type);
			
			return soundWaves[--storedSoundWaves].init(sound, sampleRate, channels, position, direction, intensity, startTime, lifeTime, loop, type);
		}
		
		public static void destroy(SoundWave soundWave) {
			if (storedSoundWaves < soundWaves.Length) soundWaves[storedSoundWaves++] = soundWave;
		}
	}
}

