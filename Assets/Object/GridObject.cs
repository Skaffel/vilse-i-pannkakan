using Vilse;
using UnityEngine;
using System;
using System.Collections;

public class GridObject : MonoBehaviour {
	public Vector2 position;
	public Vector2 direction;
	
	private Grid grid;
	
	// Use this for initialization
	void Awake () {
		WorldGenerator worldGenerator = Utilities.getComponent<WorldGenerator>("World");
		grid = worldGenerator.getGrid();
		
		update();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		update();
	}
	
	protected void update() {
		position.x = transform.position.x / grid.cellSize.x;
		position.y = transform.position.z / grid.cellSize.y;
		
		direction.x = Mathf.Sin(transform.localEulerAngles.y * Mathf.PI / 90);
		direction.y = Mathf.Cos(transform.localEulerAngles.y * Mathf.PI / 90);
		
		if (position.x < 0) position.x = 0;
		if (position.x > grid.size.x - 1) position.x = grid.size.x - 1;
		
		if (position.y < 0) position.y = 0;
		if (position.y > grid.size.y - 1) position.y = grid.size.y - 1;
	}
}

