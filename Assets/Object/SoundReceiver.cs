using Vilse;
using UnityEngine;
using System.Collections.Generic;

public class SoundReceiver : MonoBehaviour {
	public int type;
	
	protected SoundHandler soundHandler;	
	protected Grid grid;
	protected GridObject obj;
	
	// Use this for initialization
	void Start () {
		GameObject worldGenerator = GameObject.Find("World");
		grid = worldGenerator.GetComponent<WorldGenerator>().getGrid();
		
		soundHandler = GetComponent<SoundHandler>();
		obj = GetComponent<GridObject>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (soundHandler == null)
			return;
		
		Vector2 basePosition = new Vector2(Mathf.Round(obj.position.x), Mathf.Round(obj.position.y));
		Vector2 dispX = new Vector2(obj.position.x, basePosition.y);
		Vector2 dispY = new Vector2(basePosition.x, obj.position.y);
		
		Vector2 directionX = (dispX - basePosition).normalized;
		Vector2 directionY = (dispY - basePosition).normalized;
		
		double magnitudeX = (dispX - basePosition).magnitude;
		double magnitudeY = (dispY - basePosition).magnitude;
		
		if (!grid.hasDirection(basePosition, directionX))
			magnitudeX = 0;
		if (!grid.hasDirection(basePosition, directionY))
			magnitudeY = 0;
		
		Cell baseCell = grid.getCell(basePosition);
		Cell cellX = null;
		Cell cellY = null;
		
		if (magnitudeX > 0)
			cellX = grid.getCell(basePosition + directionX);
		if (magnitudeY > 0)
			cellY = grid.getCell(basePosition + directionY);
		
		addCell(baseCell, 1 - magnitudeX - magnitudeY);
		addCell(cellX, magnitudeX);
		addCell(cellY, magnitudeY);
		
		if (soundHandler != null)
			soundHandler.update();
	}
	
	private void addCell(Cell cell, double magnitude) {
		if (cell == null)
			return;
		
		foreach (SoundWave soundWave in cell.soundWaves) {
			if ((soundWave.type == 0 || soundWave.type == type)) {
				SoundWave	interpolatedSoundWave = soundWave.clone();
							interpolatedSoundWave.intensity *= magnitude;
				
				soundHandler.addSoundWave(interpolatedSoundWave);
			}
		}
		foreach (SoundWave soundWave in cell.staticWaves) {
			if ((soundWave.type == 0 || soundWave.type == type)) {
				SoundWave	interpolatedSoundWave = soundWave.clone();
							interpolatedSoundWave.intensity *= magnitude;
				
				soundHandler.addSoundWave(interpolatedSoundWave);
			}
		}
	}
}

