using Vilse;
using UnityEngine;
using System.Collections;

public class SoundEmitter : MonoBehaviour {
	public AudioClip clip;
	
	public double volumeModifier = 1;
	
	protected SoundSimulation soundSimulation;
	protected GridObject obj;
	
	protected float[] samples;
	protected double length;
	protected int sampleRate;
	protected int channels;
	
	void Start() {
		Init();
	}
	
	public void Init() {
		GameObject gameObject = GameObject.Find("Simulation");
		soundSimulation = gameObject.GetComponent<SoundSimulation>();
		
		obj = GetComponent<GridObject>();
		
		if (clip) {
			samples = new float[clip.samples * clip.channels];
			length = clip.length;
			sampleRate = (int) (clip.samples / length);
			channels = clip.channels;
		
			clip.GetData(samples, 0);
		}
	}
	
	public void emitSound(SoundWave soundWave, SoundSource source) {
		if (soundWave.source == null) {
			soundWave.source = source;
			soundWave.source.startPosition = soundWave.position;
		}
		if (isOk())
			soundSimulation.addSoundWave(soundWave);
	}
	
	protected bool isOk() {
		return soundSimulation != null;
	}
	
	// Get
	public float[] getSamples() {
		return samples;
	}
	
	public double getLength() {
		return length;
	}
	
	public int getSampleRate() {
		return sampleRate;
	}
	
	public int getChannels() {
		return channels;
	}
}


