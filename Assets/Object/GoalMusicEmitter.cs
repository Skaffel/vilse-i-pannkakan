using Vilse;
using UnityEngine;
using System.Collections.Generic;

public class GoalMusicEmitter : SoundEmitter {
	public double baseIntensity = 1;
	
	private double initWait = 1;
	private bool done = false;
	
	// Echo
	private double echoMultiplier = 0;
	
	private SoundSource source;
	
	void Start() {
		Init();
	}
	
	void Update() {
		if (!done) {
			Vector2 position = Utilities.getGridPosition(obj.position);
			
			double time = Time.time + initWait;
			
			source = new SoundSource(time, echoMultiplier, volumeModifier);
			
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2( 1, 0), baseIntensity, time, double.PositiveInfinity, true, 2), source);
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2(-1, 0), baseIntensity, time, double.PositiveInfinity, true, 2), source);
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2(0,  1), baseIntensity, time, double.PositiveInfinity, true, 2), source);
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2(0, -1), baseIntensity, time, double.PositiveInfinity, true, 2), source);
			
			done = true;
		}
	}
	
	public double getEchoMultiplier() {
		return echoMultiplier;
	}
	
	public void modEchoMultiplier(double mod) {
		echoMultiplier += mod;
		
		if (echoMultiplier < 0) echoMultiplier = 0;
		if (echoMultiplier > 0.50) echoMultiplier = 0.50;
		
		if (source != null) source.intensityModifier = echoMultiplier;
	}
}


