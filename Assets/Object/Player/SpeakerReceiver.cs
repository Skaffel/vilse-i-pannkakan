using Vilse;
using System;
using UnityEngine;
using System.Collections.Generic;

public class SpeakerReceiver : SoundHandler {
	private List<SoundWave> soundWaves;
	private List<SoundWave> bufferedList;
	
	private HashSet<SoundSource> sources;
	private HashSet<SoundSource> bufferedSources;
	
	private GridObject obj;
	
	private double timeCorrection;
	
	private bool initAudio;
	private bool initCorrection;
	
	// Use this for initialization
	void Awake() {
		obj = GetComponent<GridObject>();
		
		bufferedList = new List<SoundWave>();
		bufferedSources = new HashSet<SoundSource>();
		
		initAudio = false;
		initCorrection = false;
		
		update();
		
		audio.Play();
	}
	
	void Update() {
		if (initAudio && !initCorrection) {
			timeCorrection = Time.time - AudioSettings.dspTime;
			
			initCorrection = true;
		}
	}
	
	void OnAudioFilterRead(float[] data, int channels) {
		int processedSamples = data.Length / channels;
		
		Vector2 left =  new Vector2(-obj.direction.y,  obj.direction.x);
		Vector2 right = new Vector2( obj.direction.y, -obj.direction.x);
		
		foreach (SoundWave soundWave in soundWaves) {
			soundWave.currentSample = soundWave.source.currentSample + soundWave.offsetSample;
			
			long currentSample = soundWave.currentSample;
			
			if (soundWave.loop) currentSample = currentSample % soundWave.sound.Length;
			
			float[] soundSamples = soundWave.sound;
			int sampleChannels = soundWave.channels;
			long dataStart = (currentSample < 0)? -currentSample * channels : 0;
			long startSample = (currentSample < 0)? 0 : currentSample;
			
			float intensity = (float) (soundWave.intensity * soundWave.source.volumeModifier);
			
			intensity = ((intensity > 1)? 1 : intensity) * (float) soundWave.source.intensityModifier;
			
			float channelLeftPan = Mathf.Clamp(1 - Vector2.Dot(left, soundWave.direction), 0, 1.8f) * intensity / 2;
			float channelRightPan = Mathf.Clamp(1 - Vector2.Dot(right, soundWave.direction), 0, 1.8f) * intensity / 2; 
			
			for (long dataIndex = dataStart, clipIndex = startSample * sampleChannels; 
				 dataIndex < data.Length && clipIndex < soundSamples.Length; 
				 dataIndex += channels, clipIndex += sampleChannels) {
				
				long clip = clipIndex;
				
				if (soundWave.loop) clip = clipIndex % soundSamples.Length;
				
				data[dataIndex + 0] += soundSamples[clip] * channelLeftPan;
				data[dataIndex + 1] += soundSamples[clip] * channelRightPan;
			}
			
			soundWave.currentSample += processedSamples;
		}
		
		foreach (SoundSource source in sources) {
			source.currentSample += processedSamples;
		}
		
		//Debug.Log(soundWaves.Count + " " + sources.Count);
		
		initAudio = true;
	}
	
	override public void addSoundWave(SoundWave soundWave) {
		if (!initCorrection || soundWave.sound == null)
			return;
		
		if (soundWave.source.currentSample < 0)
			soundWave.source.currentSample = (long) Math.Round(soundWave.sampleRate * (AudioSettings.dspTime + timeCorrection - soundWave.source.startTime));
		
		/*if (soundWave.currentSample < 0) 
			soundWave.offsetSample = (long) Math.Round(soundWave.sampleRate * (soundWave.source.startTime - soundWave.startTime));*/
		
		bufferedList.Add(soundWave);
		bufferedSources.Add(soundWave.source);
	}
	
	override public void update() {
		soundWaves = bufferedList;
		bufferedList = new List<SoundWave>();
		
		sources = bufferedSources;
		bufferedSources = new HashSet<SoundSource>();
	}
}

