using Vilse;
using UnityEngine;
using System.Collections;

public class BreathEmitter : MonoBehaviour {
	public AudioClip breathIn;
	public AudioClip breathOut;
	
	public double volumeModifier = 1;
	
	public double baseRate = 0.3;
	public double baseIntensity = 0.1;
	public double breathRateStaminaModifier = 2.2;
	public double breathIntensityStaminaModifier = 0.05;
	
	private SoundEmitter breathInEmitter;
	private SoundEmitter breathOutEmitter;
	
	private SoundEmitter nextEmitter;
	
	private GridObject obj;
	private PlayerObject player;
	
	private double lastTime;
	private double nextTime;
	
	// Use this for initialization
	void Start () {
		obj = GetComponent<GridObject>();
		player = GetComponent<PlayerObject>();
		
		breathInEmitter = gameObject.AddComponent<SoundEmitter>();
		breathOutEmitter = gameObject.AddComponent<SoundEmitter>();
		
		breathInEmitter.clip = breathIn;
		breathOutEmitter.clip = breathOut;
		
		breathInEmitter.volumeModifier = volumeModifier;
		breathOutEmitter.volumeModifier = volumeModifier;
		
		lastTime = Time.time;
		
		nextEmitter = breathInEmitter;
	}
	
	// Update is called once per frame
	void Update () {
		nextTime = (player.forceBreath())? Time.time : lastTime + baseRate + player.getStamina() * breathRateStaminaModifier;
		
		if (Time.time > nextTime - Utilities.getBufferLength() && player.isBreathing() || player.forceBreath() && Time.time > lastTime + 0.7) {
			double intensity = baseIntensity - player.getStamina() * breathIntensityStaminaModifier;
			
			Vector2 position = Utilities.getGridPosition(obj.position);
			
			SoundSource source = new SoundSource(nextTime, 1, volumeModifier); 
			
			nextEmitter.emitSound(new SoundWave(nextEmitter.getSamples(), nextEmitter.getSampleRate(), nextEmitter.getChannels(), position, new Vector2( 1, 0), intensity, nextTime, nextEmitter.getLength(), false, 0), source);
			nextEmitter.emitSound(new SoundWave(nextEmitter.getSamples(), nextEmitter.getSampleRate(), nextEmitter.getChannels(), position, new Vector2(-1, 0), intensity, nextTime, nextEmitter.getLength(), false, 0), source);
			nextEmitter.emitSound(new SoundWave(nextEmitter.getSamples(), nextEmitter.getSampleRate(), nextEmitter.getChannels(), position, new Vector2(0,  1), intensity, nextTime, nextEmitter.getLength(), false, 0), source);
			nextEmitter.emitSound(new SoundWave(nextEmitter.getSamples(), nextEmitter.getSampleRate(), nextEmitter.getChannels(), position, new Vector2(0, -1), intensity, nextTime, nextEmitter.getLength(), false, 0), source);
			
			lastTime = nextTime;
			
			if (nextEmitter == breathInEmitter) nextEmitter = breathOutEmitter;
			else 								nextEmitter = breathInEmitter;
		}
		
		player.setForceBreath(false);
	}
}
