using Vilse;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public double baseSpeed = 100;
	public double baseThrust = 100;
	
	public double walkStaminaCharge = 0.03;
	
	private PlayerObject player;
	
	private double speed = 0;
	private double thrust = 0;
	private double staminaCharge = 0;
	
	private Vector2 direction;
	
	// Use this for initialization
	void Start () {
		direction = new Vector2();
		
		player = GetComponent<PlayerObject>();
		
		mod(1, 1, 1);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		direction.Set(0, 0);
		direction.Set(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		direction.Normalize();
		
		if (direction.magnitude > 0) player.modStamina(-staminaCharge * Time.fixedDeltaTime);
		if (direction.x < 0) direction.x *= 0.5f;
		
		Vector2 worldDirection = new Vector2();
		
		worldDirection.x = Mathf.Sin(transform.localEulerAngles.y * Mathf.PI / 90) * direction.y + Mathf.Cos(transform.localEulerAngles.y * Mathf.PI / 90) * direction.x;
		worldDirection.y = -Mathf.Sin(transform.localEulerAngles.y * Mathf.PI / 90) * direction.x + Mathf.Cos(transform.localEulerAngles.y * Mathf.PI / 90) * direction.y;
		
		Vector2 vel = new Vector2(rigidbody.velocity.x, rigidbody.velocity.z);
		
		Vector2 force = worldDirection * (float) thrust;
		Vector2 damping = -vel / (float) speed;
		
		if (direction.magnitude == 0 && vel.magnitude < 50) {
			rigidbody.velocity = new Vector3(vel.x * 0.5f, 0.0f, vel.y * 0.5f);
		}
		else {
			rigidbody.AddForce(new Vector3(force.x, 0, force.y));
			rigidbody.AddForce(new Vector3(damping.x, 0, damping.y));
		}
	}
	
	public void mod(double modSpeed, double modThrust, double modStaminaCharge) {
		this.speed = baseSpeed * modSpeed;
		this.thrust = baseThrust * modThrust;
		this.staminaCharge = walkStaminaCharge * modStaminaCharge;
	}
}
