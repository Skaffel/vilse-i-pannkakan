using UnityEngine;
using System.Collections;

public class StaminaController : MonoBehaviour {
	public double staminaRefreshRate = 0.05;
	public double staminaRefreshRateRate = 0.1;
	
	private PlayerObject player;
	
	// Use this for initialization
	void Start () {
		player = GetComponent<PlayerObject>();
	}
	
	// Update is called once per frame
	void Update () {
		player.modStamina(staminaRefreshRate * Time.deltaTime);
		player.modStamina(player.getStamina() * staminaRefreshRateRate * Time.deltaTime);
		
		//Debug.Log(player.getStamina());
	}
}
