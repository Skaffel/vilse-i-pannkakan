using UnityEngine;
using System.Collections;

public class StateController : MonoBehaviour {
	public double sprintSpeedMultiplier = 1;
	public double sprintThrustMultiplier = 2;
	public double sprintStaminaMultiplier = 10;
	
	public double stealthSpeedMultiplier = 1;
	public double stealthThrustMultiplier = 0.2;
	public double stealthStaminaDrain = 0.2;
	
	public double minStaminaActivation = 0.4;
	
	private PlayerObject player;
	private PlayerController playerController; 
	
	private bool shift = false;
	private bool ctrl = false;
	
	// Use this for initialization
	void Start () {
		player = GetComponent<PlayerObject>();
		playerController = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		playerController.mod(1, 1, 1);
		
		if (Input.GetKeyDown(KeyCode.LeftShift)) shift = true;
		else if (Input.GetKeyUp(KeyCode.LeftShift)) shift = false;
		
		if (Input.GetKeyDown(KeyCode.LeftControl)) {
			ctrl = true;
			
			if (ctrl && !shift && player.getStamina() > minStaminaActivation) {
				Debug.Log("Stop breathing");
				
				player.setForceBreath(true);
				player.setBreathing(false);
			}
		}
		else if (Input.GetKeyUp(KeyCode.LeftControl)) {
			ctrl = false;
			
			if (!player.isBreathing()) {
				Debug.Log("Breathing again");
				
				player.setBreathing(true);
				player.setForceBreath(true);
			}
		}
		
		if (shift && !ctrl && player.isBreathing() && player.getStamina() > 0.1) {
			playerController.mod(sprintSpeedMultiplier, sprintThrustMultiplier, sprintStaminaMultiplier);
		}
		if (ctrl && !shift && !player.isBreathing()) {
			player.modStamina(-stealthStaminaDrain * Time.deltaTime);
			
			if ( player.getStamina() > 0) {		
				playerController.mod(stealthSpeedMultiplier, stealthThrustMultiplier, 1);
			}
			else {
				player.setBreathing(true);
				player.setForceBreath(true);
			}
		}
	}
}
