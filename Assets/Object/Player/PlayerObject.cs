using UnityEngine;
using System.Collections;

public class PlayerObject : MonoBehaviour {
	// Alive
	private bool bIsAlive = true;
	public void setAlive(bool isAlive) {this.bIsAlive = isAlive; }
	public bool isAlive() {return this.bIsAlive; }
	
	private bool bHasWon = false;
	public void setHasWon(bool hasWon) {this.bHasWon = hasWon; }
	public bool hasWon() {return this.bHasWon; }
	
	// Stamina
	private double stamina = 1;
	
	public double getStamina() { return stamina; }
	public void setStamina(double stamina) { 
		this.stamina = stamina;
		
		if (this.stamina < 0) this.stamina = 0;
		if (this.stamina > 1) this.stamina = 1;
	}
	public void modStamina(double mod) {
		setStamina(this.stamina + mod);
	}
	
	// Breath
	private bool bIsBreathing = true;
	private bool bForceBreath = true;
	
	public void setBreathing(bool isBreathing) { this.bIsBreathing = isBreathing; }
	public bool isBreathing() { return bIsBreathing; }
	
	public void setForceBreath(bool force) {this.bForceBreath = force; }
	public bool forceBreath() { return bForceBreath; }
}
