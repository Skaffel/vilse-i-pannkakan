using UnityEngine;
using System.Collections;

public class WinDeathListener : MonoBehaviour {
	private PlayerObject player;
	
	// Use this for initialization
	void Start () {
		player = GetComponent<PlayerObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider collider) {
		if (player.isAlive() && !player.hasWon() && collider.gameObject.tag == "Enemy") {
			Debug.Log("You died!");
			
			player.setAlive(false);
			
			Application.Quit();
		}
		if (player.isAlive() && !player.hasWon() && collider.gameObject.tag == "Goal") {
			Debug.Log("You won!");
			
			player.setHasWon(true);
			
			Application.Quit();
		}
	}
}
