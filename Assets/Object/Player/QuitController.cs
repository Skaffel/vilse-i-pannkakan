using UnityEngine;
using System.Collections;

public class QuitController : MonoBehaviour {
    
	public float showSplashTimeout = 3.0F;
    private bool allowQuitting = false;
	
    void Awake() {
        DontDestroyOnLoad(this.gameObject);
    }
	
	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}
    
	void OnApplicationQuit() {
        if (Application.loadedLevelName != "ExtiScene")
            StartCoroutine("DelayedQuit");
        
        if (!allowQuitting)
            Application.CancelQuit();
    }
	
    IEnumerator DelayedQuit() {
        Application.LoadLevel("ExtiScene");
		
        yield return new WaitForSeconds(showSplashTimeout);
       
		allowQuitting = true;
      
		Application.Quit();
    }
}