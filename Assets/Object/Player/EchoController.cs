using Vilse;
using UnityEngine;
using System.Collections;

public class EchoController : SoundEmitter {
	public double echoMultiplierRate = 0.1;
	public double echoInterval = 0.2;
	public double echoIntensityModifier = 1;
	
	private GoalMusicEmitter goal;
	
	private double lastTime;
	
	private double direction = -1;
	
	void Start () {
		Init();
		
		goal = Utilities.getComponent<GoalMusicEmitter>("Goal");
		
		lastTime = Time.time - echoInterval;
	}
	
	void Update () {
		if (goal == null)
			return;
		
		// Update echo multiplier
		direction = (Input.GetKeyDown(KeyCode.F))?  1 : direction;
		direction = (Input.GetKeyUp  (KeyCode.F))? -0.5 : direction;
		
		goal.modEchoMultiplier(echoMultiplierRate * Time.deltaTime * direction);
		
		double nextTime = lastTime + echoInterval;
		
		if (goal.getEchoMultiplier() > 0 && Time.time > nextTime - Utilities.getBufferLength()) {
			Vector2 position = Utilities.getGridPosition(obj.position);
			
			SoundSource source = new SoundSource(Time.time,1, volumeModifier);
			
			emitSound(new SoundWave(null, 0, 0, position, new Vector2( 1, 0), goal.getEchoMultiplier() * echoIntensityModifier, Time.time, echoInterval / 10, false, 1), source);
			emitSound(new SoundWave(null, 0, 0, position, new Vector2(-1, 0), goal.getEchoMultiplier() * echoIntensityModifier, Time.time, echoInterval / 10, false, 1), source);
			emitSound(new SoundWave(null, 0, 0, position, new Vector2(0,  1), goal.getEchoMultiplier() * echoIntensityModifier, Time.time, echoInterval / 10, false, 1), source);
			emitSound(new SoundWave(null, 0, 0, position, new Vector2(0, -1), goal.getEchoMultiplier() * echoIntensityModifier, Time.time, echoInterval / 10, false, 1), source);
			
			lastTime = nextTime;
		}
	}
}
