using Vilse;
using UnityEngine;
using System.Collections.Generic;

public abstract class SoundHandler : MonoBehaviour {
	abstract public void addSoundWave(SoundWave soundWave);
	abstract public void update();
}

