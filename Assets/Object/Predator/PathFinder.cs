using Vilse;
using UnityEngine;
using System.Collections.Generic;

public class PathFinder : MonoBehaviour {
	private Grid grid;
	
	private GridObject obj;
	private PredatorObject predator;
	
	private Cell oldCell;
	private Cell nextCell;
	
	private Vector2 oldPosition;
	
	// Use this for initialization
	void Start () {
		GameObject worldGenerator = GameObject.Find("World");
		
		grid = worldGenerator.GetComponent<WorldGenerator>().getGrid();
		
		obj = GetComponent<GridObject>();
		predator = GetComponent<PredatorObject>();
		
		oldCell = null;
		nextCell = null;
		
		predator.setDirection(false);
		
		oldPosition = new Vector2(obj.position.x, obj.position.y);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (nextCell != null) {
			
			Vector2 moved = obj.position - oldPosition;
			Vector2 dist = nextCell.position - oldPosition;
			
			if (moved.magnitude >= dist.magnitude - 0.1 || 2 < dist.magnitude) {
				nextCell = null;
				
				predator.setDirection(false);
			}
		}
		
		oldPosition.Set(obj.position.x, obj.position.y);
		
		int itr = 0;
		while (nextCell == null && itr < 10) {
			List<Vector2> directions = grid.getDirections(obj.position);
			
			int choice = Random.Range(0, directions.Count);
			
			if (directions.Count > 0 && choice < directions.Count) {
				Cell newCell = grid.getCell(obj.position + directions[choice]);
				
				bool isBack = false;
				if (oldCell != null) {
					isBack = newCell.position.x == oldCell.position.x && newCell.position.y == oldCell.position.y;
				} 
				
				if (!isBack || directions.Count <= 1) {
					nextCell = newCell;
					
					oldCell = grid.getCell(obj.position);
					
					predator.setDirection(false);
				}
			}
			
			itr++;
		}
	}
	
	public Vector2 getDirection() {
		if (nextCell == null)
			return new Vector2();
		return (nextCell.position - obj.position).normalized;
	}
	
	public bool setNewDirection(Vector2 direction, Vector2 position) {
		if (!grid.hasDirection(obj.position, direction))
			return false;
		if (grid.getCell(position) == grid.getCell(obj.position))
			return true;
		
		nextCell = grid.getCell(obj.position + direction);
		
		predator.setDirection(true);
		
		return true;
	}
}
