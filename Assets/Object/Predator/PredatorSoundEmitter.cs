using Vilse;
using UnityEngine;
using System.Collections;

public class PredatorSoundEmitter : SoundEmitter {
	public double baseInterval = 1;
	public double baseIntensity = 0.5;
	public double aggressivityIntervalModifier = 0.1;
	public double aggressivityIntensityModifier = 0.1;
	
	protected PredatorObject predator;
	
	protected double lastTime;
	
	// Use this for initialization
	void Start () {
		Init();
		
		predator = GetComponent<PredatorObject>();
		
		lastTime = Time.time - baseInterval;
	}
	
	// Update is called once per frame
	void Update () {
		double nextTime = lastTime + baseInterval - predator.getAggressivity() * aggressivityIntervalModifier;
		
		if (isOk() && Time.time > nextTime - Utilities.getBufferLength()) {
			double intensity = baseIntensity + predator.getAggressivity() * aggressivityIntensityModifier;
			
			Vector2 position = Utilities.getGridPosition(obj.position);
			
			SoundSource source = new SoundSource(nextTime, 0.5, volumeModifier);
			
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2( 1, 0), intensity, nextTime, length, false, 2), source);
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2(-1, 0), intensity, nextTime, length, false, 2), source);
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2(0,  1), intensity, nextTime, length, false, 2), source);
			emitSound(new SoundWave(samples, sampleRate, channels, position, new Vector2(0, -1), intensity, nextTime, length, false, 2), source);
			
			lastTime = nextTime;
		}
	}
}
