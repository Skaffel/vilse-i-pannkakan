using UnityEngine;
using System.Collections;

public class PredatorController : MonoBehaviour {
	public double baseSpeed = 2;
	public double aggressivityBaseSpeedMultiplier = 6;
	
	private PredatorObject predator;
	private PathFinder pathFinder;
	
	void Start () {
		predator = GetComponent<PredatorObject>();
		pathFinder = GetComponent<PathFinder>();
	}
	
	void FixedUpdate () {
		Vector2 direction = pathFinder.getDirection();
		Vector2 velocity = direction * (float) (baseSpeed + predator.getAggressivity() * aggressivityBaseSpeedMultiplier);
		Vector3 newVelocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.y);
		
		if (direction.x == 1)
			transform.eulerAngles = new Vector3(0, 90, 0);
		else if (direction.x == -1)
			transform.eulerAngles = new Vector3(0, 270, 0);
		else if (direction.y ==  1)
			transform.eulerAngles = new Vector3(0, 0, 0);
		else if (direction.y == -1)
			transform.eulerAngles = new Vector3(0, 180, 0);
		
		rigidbody.velocity = newVelocity;
	}
}
