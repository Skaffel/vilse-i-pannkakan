using UnityEngine;
using System.Collections;

public class PredatorObject : MonoBehaviour {
	// Aggressivity
	protected double aggressivity = 0;
	public double getAggressivity() { return aggressivity; }
	
	public void setAggressivity(double aggressivity) { 
		this.aggressivity = aggressivity; 
		
		if (this.aggressivity < 0) this.aggressivity = 0;
		if (this.aggressivity > 1) this.aggressivity = 1;
	}
	
	public void modAggressivity(double mod) { 
		setAggressivity(aggressivity + mod);
	}
	
	// Pathfinding
	private bool bHasDirection = false;
	public bool hasDirection() {return this.bHasDirection; }
	public void setDirection(bool hasDirection) {this.bHasDirection = hasDirection; }
	
}
