using Vilse;
using UnityEngine;
using System.Collections;

public class AggressivityController : SoundHandler {
	public double minAggressitivityReaction = 0.1;
	public double intensityAggressitivityModifier = 1;
	public double aggressitivityDecay = 0.1;
	
	private PredatorObject predator;
	private PathFinder pathFinder;
	
	// Use this for initialization
	void Start () {
		predator = GetComponent<PredatorObject>();
		pathFinder = GetComponent<PathFinder>();
	}
	
	// Update is called once per frame
	void Update () {
		predator.modAggressivity(-aggressitivityDecay * Time.deltaTime);
		
		//Debug.Log(predator.getAggressivity());
	}
	
	override public void addSoundWave(SoundWave soundWave) {
		if (soundWave.startTime < Time.time && Time.time < soundWave.startTime + soundWave.lifeTime) {
			if (soundWave.intensity * soundWave.source.intensityModifier * intensityAggressitivityModifier > predator.getAggressivity() + minAggressitivityReaction || !predator.hasDirection()) {
				if (pathFinder.setNewDirection(-1 * soundWave.direction, soundWave.source.startPosition)) {
					if (soundWave.intensity * soundWave.source.intensityModifier * intensityAggressitivityModifier > predator.getAggressivity() + minAggressitivityReaction) {
						predator.setAggressivity(soundWave.intensity * intensityAggressitivityModifier);
					}
				}
			}
		}
	}
	
	override public void update() {}
}
